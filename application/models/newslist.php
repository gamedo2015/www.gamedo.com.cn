<?php
/**
 *新闻信息model
 **/
if(!defined('BASEPATH')) exit();

class Newslist extends CI_Model{

    function __construct(){
        parent::__construct();
    }

    /**
     * 新闻信息 获得数据
     * $array 搜索条件 array
     * $limit 查询条数
     * $offset 从第几个查
     */
    public function newsList($array,$limit,$offset)
    {
        return $this->db->select('*')->where($array)->order_by('pubTime','desc')->get('news',$limit,$offset)->result_array();
    }
    /**
     * 根据传的字段与对应值查询在对应表里的数量
     * $table 查询的表
     * $array 传的数据 array
     */
    public function existField($table,$array)
    {
        return $this->db->select('*')->where($array)->order_by('pubTime','desc')->get($table)->result_array();
    }
}