<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index_page extends MY_Controller {

	/**
	 * Index Page for this controller.
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model('newslist');
	}

	public function index()
	{
		$top = $this->newslist->existField('news',array('is_top' => 1, 'is_show' => 1, 'pubTime <=' => time()));
		$notice = $this->newslist->newsList(array('is_show' => 1, 'type ' => 2, 'pubTime <=' => time()),1,0);
		$list = $this->newslist->newsList(array('is_top !=' => 1, 'type !=' => 2,'is_show' => 1, 'pubTime <=' => time()),4,0);

		$this->smarty->assign('top',$top[0]);
		$this->smarty->assign('notice',$notice[0]);
		$this->smarty->assign('list',$list);
    $this->smarty->display('index.html');
	}

}

