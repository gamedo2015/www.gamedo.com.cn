<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mylist extends My_Controller {

	/**
	 * Index Page for this controller.
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model('newslist');
		$this->load->library('pagination');

	}
	//前台首页
	public function index()
  {
    $top = $this->newslist->existField('news',array('is_top' => 1, 'is_show' => 1, 'pubTime <=' => time()));
    $notice = $this->newslist->newsList(array('is_show' => 1, 'type ' => 2, 'pubTime <=' => time()),1,0);
    $list = $this->newslist->newsList(array('is_top !=' => 1, 'type !=' => 2,'is_show' => 1, 'pubTime <=' => time()),4,0);

    $this->smarty->assign('top',$top[0]);
    $this->smarty->assign('notice',$notice[0]);
    $this->smarty->assign('list',$list);
    $this->smarty->display('index.html');
  }
	//新闻页
	public function newlist()
	{
    $list = $this->newslist->existField('news',array('is_show' => 1, 'type !=' => 2, 'pubTime <=' => time()));
    //debug($this->db->last_query());
    $limit = 4;
    $offset = ($this->uri->segment(3,1)-1)*$limit;
    $config['base_url'] = base_url('mylist/newlist');
    $config['total_rows'] = count($list);
    $config['per_page'] = $limit;
    $config['first_link'] = '首页';
    $config['last_link'] = '尾页';
    $config['next_link'] = '下一页';
    $config['prev_link'] = '上一页';
    $this->pagination->initialize($config);
    $page=$this->pagination->create_links();

    $data = $this->newslist->newsList(array('is_show' => 1, 'type !=' => 2, 'pubTime <=' => time()),$limit,$offset);

		$this->smarty->assign('data',$data);
		$this->smarty->assign('page',$page);
    $this->smarty->display('newlist.html');
	}
  //新闻公告
  public function newsNotice()
  {
    $list = $this->newslist->existField('news',array('is_show' => 1, 'type ' => 2, 'pubTime <=' => time()));
    //debug($this->db->last_query());
    $limit = 4;
    $offset = ($this->uri->segment(3,1)-1)*$limit;
    $config['base_url'] = base_url('mylist/newsNotice');
    $config['total_rows'] = count($list);
    $config['per_page'] = $limit;
    $config['first_link'] = '首页';
    $config['last_link'] = '尾页';
    $config['next_link'] = '下一页';
    $config['prev_link'] = '上一页';
    $this->pagination->initialize($config);
    $page=$this->pagination->create_links();

    $data = $this->newslist->newsList(array('is_show' => 1, 'type ' => 2, 'pubTime <=' => time()),$limit,$offset);
    $this->smarty->assign('data',$data);
    $this->smarty->assign('page',$page);
    $this->smarty->display('newsNotice.html');
  }
	//新闻详情
	public function newscont()
	{
		$id = $this->uri->segment(3);
		$result = $this->newslist->existField('news',array('id' => $id));
		$this->smarty->assign('result',$result[0]);
    $this->smarty->display('newscont.html');
	}
	//发展历程
	public function course()
	{
    $this->smarty->display('course.html');
	}
	//产品中心
	public function info()
	{
    $this->smarty->display('info.html');
	}
	//版权合作
	public function productCenter()
	{
    $this->smarty->display('productCenter.html');
	}
	//关于掌纵
	public function guanyuzongheng()
	{
    $this->smarty->display('guanyuzongheng.html');
	}
	public function guanyuzongheng2()
	{
    $this->smarty->display('guanyuzongheng_02.html');
	}
	public function guanyuzongheng3()
	{
    $this->smarty->display('guanyuzongheng_03.html');
	}
	public function guanyuzongheng4()
	{
    $this->smarty->display('guanyuzongheng_04.html');
	}
	public function guanyuzongheng5()
	{
    $this->smarty->display('guanyuzongheng_05.html');
	}
	//联系我们
	public function about()
	{
    $this->smarty->display('about.html');
	}

}
